<?php

namespace Steam\CounterStrike\Infrastructure\Persistence;

use Doctrine\ORM\EntityManagerInterface;
use Steam\CounterStrike\Domain\PlayerId;
use Steam\CounterStrike\Domain\PlayerStats;
use Steam\CounterStrike\Domain\PlayerStatsRepository;

class OrmPlayerStatsRepository implements PlayerStatsRepository
{
    private $entityManager;
    private $ormRepository;

    public function __construct(EntityManagerInterface $entityManager, $className)
    {
        $this->entityManager = $entityManager;
        $this->ormRepository = $entityManager->getRepository($className);
    }

    public function findLastForPlayer(PlayerId $playerId)
    {
        $first = $this->ormRepository->findBy(['playerId' => $playerId->getId()], ['id' => 'DESC']);

        return isset($first[0]) ? $first[0] : null;
    }

    public function save(PlayerStats $playerStats)
    {
        $this->entityManager->transactional(function () use ($playerStats) {
            $this->entityManager->persist($playerStats);
            $this->entityManager->flush();
        });
    }

    public function findAll()
    {
        return $this->ormRepository->findAll();
    }
}
