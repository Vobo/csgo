<?php

namespace Steam\CounterStrike\Infrastructure;

use Steam\CounterStrike\Domain\PlayerId;
use Steam\CounterStrike\Domain\PlayerStats;
use Steam\CounterStrike\Domain\SteamStats;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PlayerStatsTransformer
{
    public function toPlayerStatsFromApiResponse($steamStats, PlayerId $playerId)
    {
        $playerStats = new PlayerStats($playerId, $this->normalizeToSteamStats($steamStats));

        return $playerStats;
    }

    /**
     * @param $steamStats
     *
     * @return SteamStats
     */
    private function normalizeToSteamStats($steamStats)
    {
        // @todo Look into symfony normalizer maybe there is way to skip the mapping step
        $mappedElements = [];
        foreach ($steamStats as $lastMatchElement) {
            $mappedElements[$lastMatchElement['name']] = $lastMatchElement['value'];
        }

        $normalizer = new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter(), new PropertyAccessor(true, true));

        return $normalizer->denormalize($mappedElements, 'Steam\CounterStrike\Domain\SteamStats');
    }
}
