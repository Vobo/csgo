<?php

namespace Steam\CounterStrike\Application;

use Steam\CounterStrike\Domain\PlayerId;

class FetchPlayerDataCommand
{
    private $playerId;

    public function __construct($aPlayerId)
    {
        $this->playerId = new PlayerId($aPlayerId);
    }

    public function playerId()
    {
        return $this->playerId;
    }
}
