<?php

namespace Steam\CounterStrike\Domain;

class PlayerId
{
    private $playerId;

    public function __construct($playerId)
    {
        $this->playerId = (string)$playerId;
    }

    public function getId()
    {
        return $this->playerId;
    }
}
