<?php

namespace Steam\CounterStrike\Domain;

use Exception;

class PlayerStatsContainer
{
    private $playerStats = [];

    /**
     * @param PlayerStats $playerStats
     *
     * @return SteamStats
     */
    public function getDiff(PlayerStats $playerStats)
    {
        if (!$this->hasLastPlayerStats($playerStats)) {
            $this->setPlayerStats($playerStats);

            return $playerStats->getSteamStats();
        }

        $lastPlayerStats = $this->getLastPlayerStats($playerStats);

        $this->setPlayerStats($playerStats);

        return $playerStats->getSteamStats()->subtract($lastPlayerStats);
    }

    /**
     * @param PlayerStats $playerStats
     *
     * @return bool
     */
    private function hasLastPlayerStats(PlayerStats $playerStats)
    {
        return isset($this->playerStats[$playerStats->getPlayerId()]);
    }

    /**
     * @param PlayerStats $playerStats
     *
     * @return SteamStats
     *
     * @throws Exception
     */
    private function getLastPlayerStats(PlayerStats $playerStats)
    {
        if (!$this->hasLastPlayerStats($playerStats)) {
            throw new Exception('There where no previous player stats found');
        }

        return $this->playerStats[$playerStats->getPlayerId()];
    }

    /**
     * @param PLayerStats $playerStats
     */
    private function setPlayerStats(PLayerStats $playerStats)
    {
        $this->playerStats[$playerStats->getPlayerId()] = $playerStats->getSteamStats();
    }
}