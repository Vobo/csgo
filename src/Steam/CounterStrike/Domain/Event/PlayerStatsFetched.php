<?php

namespace Steam\CounterStrike\Domain\Event;

use Steam\CounterStrike\Domain\PlayerStats;

class PlayerStatsFetched
{
    private $playerStats;

    public function __construct(PlayerStats $playerStats)
    {
        $this->playerStats = $playerStats;
    }

    public function getPlayerStats()
    {
        return $this->playerStats;
    }
}
