<?php

namespace Steam\CounterStrike\Domain;

interface PlayerStatsRepository
{
    /**
     * @param PlayerId $playerId
     *
     * @return PlayerStats|null
     */
    public function findLastForPlayer(PlayerId $playerId);

    /**
     * @return PlayerStats[]
     */
    public function findAll();

    /**
     * @param PlayerStats $playerStats
     *
     * @return void
     */
    public function save(PlayerStats $playerStats);
}
