<?php

namespace Steam\CounterStrike\Domain;

class SteamStats
{
    public $totalKills;
    public $totalDeaths;
    public $totalTimePlayed;
    public $totalPlantedBombs;
    public $totalDefusedBombs;
    public $totalWins;
    public $totalDamageDone;
    public $totalMoneyEarned;
    public $totalRescuedHostages;
    public $totalKillsKnife;
    public $totalKillsHegrenade;
    public $totalKillsGlock;
    public $totalKillsDeagle;
    public $totalKillsElite;
    public $totalKillsFiveseven;
    public $totalKillsXm1014;
    public $totalKillsMac10;
    public $totalKillsUmp45;
    public $totalKillsP90;
    public $totalKillsAwp;
    public $totalKillsAk47;
    public $totalKillsAug;
    public $totalKillsFamas;
    public $totalKillsG3sg1;
    public $totalKillsM249;
    public $totalKillsHeadshot;
    public $totalKillsEnemyWeapon;
    public $totalWinsPistolround;
    public $totalWinsMapCsOffice;
    public $totalWinsMapDeCbble;
    public $totalWinsMapDeDust2;
    public $totalWinsMapDeDust;
    public $totalWinsMapDeInferno;
    public $totalWinsMapDeNuke;
    public $totalWinsMapDeTrain;
    public $totalWeaponsDonated;
    public $totalBrokenWindows;
    public $totalKillsEnemyBlinded;
    public $totalKillsAgainstZoomedSniper;
    public $totalDominations;
    public $totalDominationOverkills;
    public $totalRevenges;
    public $totalShotsHit;
    public $totalShotsFired;
    public $totalRoundsPlayed;
    public $totalShotsDeagle;
    public $totalShotsGlock;
    public $totalShotsElite;
    public $totalShotsFiveseven;
    public $totalShotsAwp;
    public $totalShotsAk47;
    public $totalShotsAug;
    public $totalShotsFamas;
    public $totalShotsG3sg1;
    public $totalShotsP90;
    public $totalShotsMac10;
    public $totalShotsUmp45;
    public $totalShotsXm1014;
    public $totalShotsM249;
    public $totalHitsDeagle;
    public $totalHitsGlock;
    public $totalHitsElite;
    public $totalHitsFiveseven;
    public $totalHitsAwp;
    public $totalHitsAk47;
    public $totalHitsAug;
    public $totalHitsFamas;
    public $totalHitsG3sg1;
    public $totalHitsP90;
    public $totalHitsMac10;
    public $totalHitsUmp45;
    public $totalHitsXm1014;
    public $totalHitsM249;
    public $totalRoundsMapCsItaly;
    public $totalRoundsMapCsOffice;
    public $totalRoundsMapDeCbble;
    public $totalRoundsMapDeDust2;
    public $totalRoundsMapDeDust;
    public $totalRoundsMapDeInferno;
    public $totalRoundsMapDeNuke;
    public $totalRoundsMapDeTrain;
    public $lastMatchTWins;
    public $lastMatchCtWins;
    public $lastMatchWins;
    public $lastMatchMaxPlayers;
    public $lastMatchKills;
    public $lastMatchDeaths;
    public $lastMatchMvps;
    public $lastMatchFavweaponId;
    public $lastMatchFavweaponShots;
    public $lastMatchFavweaponHits;
    public $lastMatchFavweaponKills;
    public $lastMatchDamage;
    public $lastMatchMoneySpent;
    public $lastMatchDominations;
    public $lastMatchRevenges;
    public $totalMvps;
    public $totalRoundsMapDeLake;
    public $totalRoundsMapDeSafehouse;
    public $totalRoundsMapDeStmarc;
    public $totalGunGameRoundsWon;
    public $totalGunGameRoundsPlayed;
    public $totalWinsMapDeStmarc;
    public $totalWinsMapDeSafehouse;
    public $totalMatchesWon;
    public $totalMatchesPlayed;
    public $totalGgMatchesPlayed;
    public $totalProgressiveMatchesWon;
    public $totalContributionScore;
    public $lastMatchContributionScore;
    public $lastMatchRounds;
    public $totalKillsHkp2000;
    public $totalShotsHkp2000;
    public $totalHitsHkp2000;
    public $totalHitsP250;
    public $totalKillsP250;
    public $totalShotsP250;
    public $totalKillsSg556;
    public $totalShotsSg556;
    public $totalHitsSg556;
    public $totalHitsScar20;
    public $totalKillsScar20;
    public $totalShotsScar20;
    public $totalShotsSsg08;
    public $totalHitsSsg08;
    public $totalKillsSsg08;
    public $totalShotsMp7;
    public $totalHitsMp7;
    public $totalKillsMp7;
    public $totalKillsMp9;
    public $totalShotsMp9;
    public $totalHitsMp9;
    public $totalHitsNova;
    public $totalKillsNova;
    public $totalShotsNova;
    public $totalHitsNegev;
    public $totalKillsNegev;
    public $totalShotsNegev;
    public $totalShotsSawedoff;
    public $totalHitsSawedoff;
    public $totalKillsSawedoff;
    public $totalShotsBizon;
    public $totalHitsBizon;
    public $totalKillsBizon;
    public $totalKillsTec9;
    public $totalShotsTec9;
    public $totalHitsTec9;
    public $totalShotsMag7;
    public $totalHitsMag7;
    public $totalKillsMag7;
    public $totalGunGameContributionScore;
    public $lastMatchGgContributionScore;
    public $totalKillsM4a1;
    public $totalKillsGalilar;
    public $totalKillsMolotov;
    public $totalShotsM4a1;
    public $totalShotsGalilar;
    public $totalShotsTaser;
    public $totalHitsM4a1;
    public $totalHitsGalilar;
    public $totalMatchesWonSafehouse;

    public function equals($steamStats)
    {
        if (
            $this->totalKills == $steamStats->totalKills
            && $this->totalDeaths == $steamStats->totalDeaths
            && $this->totalTimePlayed == $steamStats->totalTimePlayed
            && $this->totalPlantedBombs == $steamStats->totalPlantedBombs
            && $this->totalDefusedBombs == $steamStats->totalDefusedBombs
            && $this->totalWins == $steamStats->totalWins
            && $this->totalDamageDone == $steamStats->totalDamageDone
            && $this->totalMoneyEarned == $steamStats->totalMoneyEarned
            && $this->totalRescuedHostages == $steamStats->totalRescuedHostages
            && $this->totalKillsKnife == $steamStats->totalKillsKnife
            && $this->totalKillsHegrenade == $steamStats->totalKillsHegrenade
            && $this->totalKillsGlock == $steamStats->totalKillsGlock
            && $this->totalKillsDeagle == $steamStats->totalKillsDeagle
            && $this->totalKillsElite == $steamStats->totalKillsElite
            && $this->totalKillsFiveseven == $steamStats->totalKillsFiveseven
            && $this->totalKillsXm1014 == $steamStats->totalKillsXm1014
            && $this->totalKillsMac10 == $steamStats->totalKillsMac10
            && $this->totalKillsUmp45 == $steamStats->totalKillsUmp45
            && $this->totalKillsP90 == $steamStats->totalKillsP90
            && $this->totalKillsAwp == $steamStats->totalKillsAwp
            && $this->totalKillsAk47 == $steamStats->totalKillsAk47
            && $this->totalKillsAug == $steamStats->totalKillsAug
            && $this->totalKillsFamas == $steamStats->totalKillsFamas
            && $this->totalKillsG3sg1 == $steamStats->totalKillsG3sg1
            && $this->totalKillsM249 == $steamStats->totalKillsM249
            && $this->totalKillsHeadshot == $steamStats->totalKillsHeadshot
            && $this->totalKillsEnemyWeapon == $steamStats->totalKillsEnemyWeapon
            && $this->totalWinsPistolround == $steamStats->totalWinsPistolround
            && $this->totalWinsMapCsOffice == $steamStats->totalWinsMapCsOffice
            && $this->totalWinsMapDeCbble == $steamStats->totalWinsMapDeCbble
            && $this->totalWinsMapDeDust2 == $steamStats->totalWinsMapDeDust2
            && $this->totalWinsMapDeDust == $steamStats->totalWinsMapDeDust
            && $this->totalWinsMapDeInferno == $steamStats->totalWinsMapDeInferno
            && $this->totalWinsMapDeNuke == $steamStats->totalWinsMapDeNuke
            && $this->totalWinsMapDeTrain == $steamStats->totalWinsMapDeTrain
            && $this->totalWeaponsDonated == $steamStats->totalWeaponsDonated
            && $this->totalBrokenWindows == $steamStats->totalBrokenWindows
            && $this->totalKillsEnemyBlinded == $steamStats->totalKillsEnemyBlinded
            && $this->totalKillsAgainstZoomedSniper == $steamStats->totalKillsAgainstZoomedSniper
            && $this->totalDominations == $steamStats->totalDominations
            && $this->totalDominationOverkills == $steamStats->totalDominationOverkills
            && $this->totalRevenges == $steamStats->totalRevenges
            && $this->totalShotsHit == $steamStats->totalShotsHit
            && $this->totalShotsFired == $steamStats->totalShotsFired
            && $this->totalRoundsPlayed == $steamStats->totalRoundsPlayed
            && $this->totalShotsDeagle == $steamStats->totalShotsDeagle
            && $this->totalShotsGlock == $steamStats->totalShotsGlock
            && $this->totalShotsElite == $steamStats->totalShotsElite
            && $this->totalShotsFiveseven == $steamStats->totalShotsFiveseven
            && $this->totalShotsAwp == $steamStats->totalShotsAwp
            && $this->totalShotsAk47 == $steamStats->totalShotsAk47
            && $this->totalShotsAug == $steamStats->totalShotsAug
            && $this->totalShotsFamas == $steamStats->totalShotsFamas
            && $this->totalShotsG3sg1 == $steamStats->totalShotsG3sg1
            && $this->totalShotsP90 == $steamStats->totalShotsP90
            && $this->totalShotsMac10 == $steamStats->totalShotsMac10
            && $this->totalShotsUmp45 == $steamStats->totalShotsUmp45
            && $this->totalShotsXm1014 == $steamStats->totalShotsXm1014
            && $this->totalShotsM249 == $steamStats->totalShotsM249
            && $this->totalHitsDeagle == $steamStats->totalHitsDeagle
            && $this->totalHitsGlock == $steamStats->totalHitsGlock
            && $this->totalHitsElite == $steamStats->totalHitsElite
            && $this->totalHitsFiveseven == $steamStats->totalHitsFiveseven
            && $this->totalHitsAwp == $steamStats->totalHitsAwp
            && $this->totalHitsAk47 == $steamStats->totalHitsAk47
            && $this->totalHitsAug == $steamStats->totalHitsAug
            && $this->totalHitsFamas == $steamStats->totalHitsFamas
            && $this->totalHitsG3sg1 == $steamStats->totalHitsG3sg1
            && $this->totalHitsP90 == $steamStats->totalHitsP90
            && $this->totalHitsMac10 == $steamStats->totalHitsMac10
            && $this->totalHitsUmp45 == $steamStats->totalHitsUmp45
            && $this->totalHitsXm1014 == $steamStats->totalHitsXm1014
            && $this->totalHitsM249 == $steamStats->totalHitsM249
            && $this->totalRoundsMapCsItaly == $steamStats->totalRoundsMapCsItaly
            && $this->totalRoundsMapCsOffice == $steamStats->totalRoundsMapCsOffice
            && $this->totalRoundsMapDeCbble == $steamStats->totalRoundsMapDeCbble
            && $this->totalRoundsMapDeDust2 == $steamStats->totalRoundsMapDeDust2
            && $this->totalRoundsMapDeDust == $steamStats->totalRoundsMapDeDust
            && $this->totalRoundsMapDeInferno == $steamStats->totalRoundsMapDeInferno
            && $this->totalRoundsMapDeNuke == $steamStats->totalRoundsMapDeNuke
            && $this->totalRoundsMapDeTrain == $steamStats->totalRoundsMapDeTrain
            && $this->lastMatchTWins == $steamStats->lastMatchTWins
            && $this->lastMatchCtWins == $steamStats->lastMatchCtWins
            && $this->lastMatchWins == $steamStats->lastMatchWins
            && $this->lastMatchMaxPlayers == $steamStats->lastMatchMaxPlayers
            && $this->lastMatchKills == $steamStats->lastMatchKills
            && $this->lastMatchDeaths == $steamStats->lastMatchDeaths
            && $this->lastMatchMvps == $steamStats->lastMatchMvps
            && $this->lastMatchFavweaponId == $steamStats->lastMatchFavweaponId
            && $this->lastMatchFavweaponShots == $steamStats->lastMatchFavweaponShots
            && $this->lastMatchFavweaponHits == $steamStats->lastMatchFavweaponHits
            && $this->lastMatchFavweaponKills == $steamStats->lastMatchFavweaponKills
            && $this->lastMatchDamage == $steamStats->lastMatchDamage
            && $this->lastMatchMoneySpent == $steamStats->lastMatchMoneySpent
            && $this->lastMatchDominations == $steamStats->lastMatchDominations
            && $this->lastMatchRevenges == $steamStats->lastMatchRevenges
            && $this->totalMvps == $steamStats->totalMvps
            && $this->totalRoundsMapDeLake == $steamStats->totalRoundsMapDeLake
            && $this->totalRoundsMapDeSafehouse == $steamStats->totalRoundsMapDeSafehouse
            && $this->totalRoundsMapDeStmarc == $steamStats->totalRoundsMapDeStmarc
            && $this->totalGunGameRoundsWon == $steamStats->totalGunGameRoundsWon
            && $this->totalGunGameRoundsPlayed == $steamStats->totalGunGameRoundsPlayed
            && $this->totalWinsMapDeStmarc == $steamStats->totalWinsMapDeStmarc
            && $this->totalWinsMapDeSafehouse == $steamStats->totalWinsMapDeSafehouse
            && $this->totalMatchesWon == $steamStats->totalMatchesWon
            && $this->totalMatchesPlayed == $steamStats->totalMatchesPlayed
            && $this->totalGgMatchesPlayed == $steamStats->totalGgMatchesPlayed
            && $this->totalProgressiveMatchesWon == $steamStats->totalProgressiveMatchesWon
            && $this->totalContributionScore == $steamStats->totalContributionScore
            && $this->lastMatchContributionScore == $steamStats->lastMatchContributionScore
            && $this->lastMatchRounds == $steamStats->lastMatchRounds
            && $this->totalKillsHkp2000 == $steamStats->totalKillsHkp2000
            && $this->totalShotsHkp2000 == $steamStats->totalShotsHkp2000
            && $this->totalHitsHkp2000 == $steamStats->totalHitsHkp2000
            && $this->totalHitsP250 == $steamStats->totalHitsP250
            && $this->totalKillsP250 == $steamStats->totalKillsP250
            && $this->totalShotsP250 == $steamStats->totalShotsP250
            && $this->totalKillsSg556 == $steamStats->totalKillsSg556
            && $this->totalShotsSg556 == $steamStats->totalShotsSg556
            && $this->totalHitsSg556 == $steamStats->totalHitsSg556
            && $this->totalHitsScar20 == $steamStats->totalHitsScar20
            && $this->totalKillsScar20 == $steamStats->totalKillsScar20
            && $this->totalShotsScar20 == $steamStats->totalShotsScar20
            && $this->totalShotsSsg08 == $steamStats->totalShotsSsg08
            && $this->totalHitsSsg08 == $steamStats->totalHitsSsg08
            && $this->totalKillsSsg08 == $steamStats->totalKillsSsg08
            && $this->totalShotsMp7 == $steamStats->totalShotsMp7
            && $this->totalHitsMp7 == $steamStats->totalHitsMp7
            && $this->totalKillsMp7 == $steamStats->totalKillsMp7
            && $this->totalKillsMp9 == $steamStats->totalKillsMp9
            && $this->totalShotsMp9 == $steamStats->totalShotsMp9
            && $this->totalHitsMp9 == $steamStats->totalHitsMp9
            && $this->totalHitsNova == $steamStats->totalHitsNova
            && $this->totalKillsNova == $steamStats->totalKillsNova
            && $this->totalShotsNova == $steamStats->totalShotsNova
            && $this->totalHitsNegev == $steamStats->totalHitsNegev
            && $this->totalKillsNegev == $steamStats->totalKillsNegev
            && $this->totalShotsNegev == $steamStats->totalShotsNegev
            && $this->totalShotsSawedoff == $steamStats->totalShotsSawedoff
            && $this->totalHitsSawedoff == $steamStats->totalHitsSawedoff
            && $this->totalKillsSawedoff == $steamStats->totalKillsSawedoff
            && $this->totalShotsBizon == $steamStats->totalShotsBizon
            && $this->totalHitsBizon == $steamStats->totalHitsBizon
            && $this->totalKillsBizon == $steamStats->totalKillsBizon
            && $this->totalKillsTec9 == $steamStats->totalKillsTec9
            && $this->totalShotsTec9 == $steamStats->totalShotsTec9
            && $this->totalHitsTec9 == $steamStats->totalHitsTec9
            && $this->totalShotsMag7 == $steamStats->totalShotsMag7
            && $this->totalHitsMag7 == $steamStats->totalHitsMag7
            && $this->totalKillsMag7 == $steamStats->totalKillsMag7
            && $this->totalGunGameContributionScore == $steamStats->totalGunGameContributionScore
            && $this->lastMatchGgContributionScore == $steamStats->lastMatchGgContributionScore
            && $this->totalKillsM4a1 == $steamStats->totalKillsM4a1
            && $this->totalKillsGalilar == $steamStats->totalKillsGalilar
            && $this->totalKillsMolotov == $steamStats->totalKillsMolotov
            && $this->totalShotsM4a1 == $steamStats->totalShotsM4a1
            && $this->totalShotsGalilar == $steamStats->totalShotsGalilar
            && $this->totalShotsTaser == $steamStats->totalShotsTaser
            && $this->totalHitsM4a1 == $steamStats->totalHitsM4a1
            && $this->totalHitsGalilar == $steamStats->totalHitsGalilar
            && $this->totalMatchesWonSafehouse == $steamStats->totalMatchesWonSafehouse
        ) {
            return true;
        }

        return false;
    }

    public function subtract($subtract)
    {
        $newStats = new SteamStats();

        $newStats->totalKills = $this->totalKills - $subtract->totalKills;
        $newStats->totalDeaths = $this->totalDeaths - $subtract->totalDeaths;
        $newStats->totalTimePlayed = $this->totalTimePlayed - $subtract->totalTimePlayed;
        $newStats->totalPlantedBombs = $this->totalPlantedBombs - $subtract->totalPlantedBombs;
        $newStats->totalDefusedBombs = $this->totalDefusedBombs - $subtract->totalDefusedBombs;
        $newStats->totalWins = $this->totalWins - $subtract->totalWins;
        $newStats->totalDamageDone = $this->totalDamageDone - $subtract->totalDamageDone;
        $newStats->totalMoneyEarned = $this->totalMoneyEarned - $subtract->totalMoneyEarned;
        $newStats->totalRescuedHostages = $this->totalRescuedHostages - $subtract->totalRescuedHostages;
        $newStats->totalKillsKnife = $this->totalKillsKnife - $subtract->totalKillsKnife;
        $newStats->totalKillsHegrenade = $this->totalKillsHegrenade - $subtract->totalKillsHegrenade;
        $newStats->totalKillsGlock = $this->totalKillsGlock - $subtract->totalKillsGlock;
        $newStats->totalKillsDeagle = $this->totalKillsDeagle - $subtract->totalKillsDeagle;
        $newStats->totalKillsElite = $this->totalKillsElite - $subtract->totalKillsElite;
        $newStats->totalKillsFiveseven = $this->totalKillsFiveseven - $subtract->totalKillsFiveseven;
        $newStats->totalKillsXm1014 = $this->totalKillsXm1014 - $subtract->totalKillsXm1014;
        $newStats->totalKillsMac10 = $this->totalKillsMac10 - $subtract->totalKillsMac10;
        $newStats->totalKillsUmp45 = $this->totalKillsUmp45 - $subtract->totalKillsUmp45;
        $newStats->totalKillsP90 = $this->totalKillsP90 - $subtract->totalKillsP90;
        $newStats->totalKillsAwp = $this->totalKillsAwp - $subtract->totalKillsAwp;
        $newStats->totalKillsAk47 = $this->totalKillsAk47 - $subtract->totalKillsAk47;
        $newStats->totalKillsAug = $this->totalKillsAug - $subtract->totalKillsAug;
        $newStats->totalKillsFamas = $this->totalKillsFamas - $subtract->totalKillsFamas;
        $newStats->totalKillsG3sg1 = $this->totalKillsG3sg1 - $subtract->totalKillsG3sg1;
        $newStats->totalKillsM249 = $this->totalKillsM249 - $subtract->totalKillsM249;
        $newStats->totalKillsHeadshot = $this->totalKillsHeadshot - $subtract->totalKillsHeadshot;
        $newStats->totalKillsEnemyWeapon = $this->totalKillsEnemyWeapon - $subtract->totalKillsEnemyWeapon;
        $newStats->totalWinsPistolround = $this->totalWinsPistolround - $subtract->totalWinsPistolround;
        $newStats->totalWinsMapCsOffice = $this->totalWinsMapCsOffice - $subtract->totalWinsMapCsOffice;
        $newStats->totalWinsMapDeCbble = $this->totalWinsMapDeCbble - $subtract->totalWinsMapDeCbble;
        $newStats->totalWinsMapDeDust2 = $this->totalWinsMapDeDust2 - $subtract->totalWinsMapDeDust2;
        $newStats->totalWinsMapDeDust = $this->totalWinsMapDeDust - $subtract->totalWinsMapDeDust;
        $newStats->totalWinsMapDeInferno = $this->totalWinsMapDeInferno - $subtract->totalWinsMapDeInferno;
        $newStats->totalWinsMapDeNuke = $this->totalWinsMapDeNuke - $subtract->totalWinsMapDeNuke;
        $newStats->totalWinsMapDeTrain = $this->totalWinsMapDeTrain - $subtract->totalWinsMapDeTrain;
        $newStats->totalWeaponsDonated = $this->totalWeaponsDonated - $subtract->totalWeaponsDonated;
        $newStats->totalBrokenWindows = $this->totalBrokenWindows - $subtract->totalBrokenWindows;
        $newStats->totalKillsEnemyBlinded = $this->totalKillsEnemyBlinded - $subtract->totalKillsEnemyBlinded;
        $newStats->totalKillsAgainstZoomedSniper = $this->totalKillsAgainstZoomedSniper - $subtract->totalKillsAgainstZoomedSniper;
        $newStats->totalDominations = $this->totalDominations - $subtract->totalDominations;
        $newStats->totalDominationOverkills = $this->totalDominationOverkills - $subtract->totalDominationOverkills;
        $newStats->totalRevenges = $this->totalRevenges - $subtract->totalRevenges;
        $newStats->totalShotsHit = $this->totalShotsHit - $subtract->totalShotsHit;
        $newStats->totalShotsFired = $this->totalShotsFired - $subtract->totalShotsFired;
        $newStats->totalRoundsPlayed = $this->totalRoundsPlayed - $subtract->totalRoundsPlayed;
        $newStats->totalShotsDeagle = $this->totalShotsDeagle - $subtract->totalShotsDeagle;
        $newStats->totalShotsGlock = $this->totalShotsGlock - $subtract->totalShotsGlock;
        $newStats->totalShotsElite = $this->totalShotsElite - $subtract->totalShotsElite;
        $newStats->totalShotsFiveseven = $this->totalShotsFiveseven - $subtract->totalShotsFiveseven;
        $newStats->totalShotsAwp = $this->totalShotsAwp - $subtract->totalShotsAwp;
        $newStats->totalShotsAk47 = $this->totalShotsAk47 - $subtract->totalShotsAk47;
        $newStats->totalShotsAug = $this->totalShotsAug - $subtract->totalShotsAug;
        $newStats->totalShotsFamas = $this->totalShotsFamas - $subtract->totalShotsFamas;
        $newStats->totalShotsG3sg1 = $this->totalShotsG3sg1 - $subtract->totalShotsG3sg1;
        $newStats->totalShotsP90 = $this->totalShotsP90 - $subtract->totalShotsP90;
        $newStats->totalShotsMac10 = $this->totalShotsMac10 - $subtract->totalShotsMac10;
        $newStats->totalShotsUmp45 = $this->totalShotsUmp45 - $subtract->totalShotsUmp45;
        $newStats->totalShotsXm1014 = $this->totalShotsXm1014 - $subtract->totalShotsXm1014;
        $newStats->totalShotsM249 = $this->totalShotsM249 - $subtract->totalShotsM249;
        $newStats->totalHitsDeagle = $this->totalHitsDeagle - $subtract->totalHitsDeagle;
        $newStats->totalHitsGlock = $this->totalHitsGlock - $subtract->totalHitsGlock;
        $newStats->totalHitsElite = $this->totalHitsElite - $subtract->totalHitsElite;
        $newStats->totalHitsFiveseven = $this->totalHitsFiveseven - $subtract->totalHitsFiveseven;
        $newStats->totalHitsAwp = $this->totalHitsAwp - $subtract->totalHitsAwp;
        $newStats->totalHitsAk47 = $this->totalHitsAk47 - $subtract->totalHitsAk47;
        $newStats->totalHitsAug = $this->totalHitsAug - $subtract->totalHitsAug;
        $newStats->totalHitsFamas = $this->totalHitsFamas - $subtract->totalHitsFamas;
        $newStats->totalHitsG3sg1 = $this->totalHitsG3sg1 - $subtract->totalHitsG3sg1;
        $newStats->totalHitsP90 = $this->totalHitsP90 - $subtract->totalHitsP90;
        $newStats->totalHitsMac10 = $this->totalHitsMac10 - $subtract->totalHitsMac10;
        $newStats->totalHitsUmp45 = $this->totalHitsUmp45 - $subtract->totalHitsUmp45;
        $newStats->totalHitsXm1014 = $this->totalHitsXm1014 - $subtract->totalHitsXm1014;
        $newStats->totalHitsM249 = $this->totalHitsM249 - $subtract->totalHitsM249;
        $newStats->totalRoundsMapCsItaly = $this->totalRoundsMapCsItaly - $subtract->totalRoundsMapCsItaly;
        $newStats->totalRoundsMapCsOffice = $this->totalRoundsMapCsOffice - $subtract->totalRoundsMapCsOffice;
        $newStats->totalRoundsMapDeCbble = $this->totalRoundsMapDeCbble - $subtract->totalRoundsMapDeCbble;
        $newStats->totalRoundsMapDeDust2 = $this->totalRoundsMapDeDust2 - $subtract->totalRoundsMapDeDust2;
        $newStats->totalRoundsMapDeDust = $this->totalRoundsMapDeDust - $subtract->totalRoundsMapDeDust;
        $newStats->totalRoundsMapDeInferno = $this->totalRoundsMapDeInferno - $subtract->totalRoundsMapDeInferno;
        $newStats->totalRoundsMapDeNuke = $this->totalRoundsMapDeNuke - $subtract->totalRoundsMapDeNuke;
        $newStats->totalRoundsMapDeTrain = $this->totalRoundsMapDeTrain - $subtract->totalRoundsMapDeTrain;
        $newStats->lastMatchTWins = $this->lastMatchTWins - $subtract->lastMatchTWins;
        $newStats->lastMatchCtWins = $this->lastMatchCtWins - $subtract->lastMatchCtWins;
        $newStats->lastMatchWins = $this->lastMatchWins - $subtract->lastMatchWins;
        $newStats->lastMatchMaxPlayers = $this->lastMatchMaxPlayers - $subtract->lastMatchMaxPlayers;
        $newStats->lastMatchKills = $this->lastMatchKills - $subtract->lastMatchKills;
        $newStats->lastMatchDeaths = $this->lastMatchDeaths - $subtract->lastMatchDeaths;
        $newStats->lastMatchMvps = $this->lastMatchMvps - $subtract->lastMatchMvps;
        $newStats->lastMatchFavweaponId = $this->lastMatchFavweaponId - $subtract->lastMatchFavweaponId;
        $newStats->lastMatchFavweaponShots = $this->lastMatchFavweaponShots - $subtract->lastMatchFavweaponShots;
        $newStats->lastMatchFavweaponHits = $this->lastMatchFavweaponHits - $subtract->lastMatchFavweaponHits;
        $newStats->lastMatchFavweaponKills = $this->lastMatchFavweaponKills - $subtract->lastMatchFavweaponKills;
        $newStats->lastMatchDamage = $this->lastMatchDamage - $subtract->lastMatchDamage;
        $newStats->lastMatchMoneySpent = $this->lastMatchMoneySpent - $subtract->lastMatchMoneySpent;
        $newStats->lastMatchDominations = $this->lastMatchDominations - $subtract->lastMatchDominations;
        $newStats->lastMatchRevenges = $this->lastMatchRevenges - $subtract->lastMatchRevenges;
        $newStats->totalMvps = $this->totalMvps - $subtract->totalMvps;
        $newStats->totalRoundsMapDeLake = $this->totalRoundsMapDeLake - $subtract->totalRoundsMapDeLake;
        $newStats->totalRoundsMapDeSafehouse = $this->totalRoundsMapDeSafehouse - $subtract->totalRoundsMapDeSafehouse;
        $newStats->totalRoundsMapDeStmarc = $this->totalRoundsMapDeStmarc - $subtract->totalRoundsMapDeStmarc;
        $newStats->totalGunGameRoundsWon = $this->totalGunGameRoundsWon - $subtract->totalGunGameRoundsWon;
        $newStats->totalGunGameRoundsPlayed = $this->totalGunGameRoundsPlayed - $subtract->totalGunGameRoundsPlayed;
        $newStats->totalWinsMapDeStmarc = $this->totalWinsMapDeStmarc - $subtract->totalWinsMapDeStmarc;
        $newStats->totalWinsMapDeSafehouse = $this->totalWinsMapDeSafehouse - $subtract->totalWinsMapDeSafehouse;
        $newStats->totalMatchesWon = $this->totalMatchesWon - $subtract->totalMatchesWon;
        $newStats->totalMatchesPlayed = $this->totalMatchesPlayed - $subtract->totalMatchesPlayed;
        $newStats->totalGgMatchesPlayed = $this->totalGgMatchesPlayed - $subtract->totalGgMatchesPlayed;
        $newStats->totalProgressiveMatchesWon = $this->totalProgressiveMatchesWon - $subtract->totalProgressiveMatchesWon;
        $newStats->totalContributionScore = $this->totalContributionScore - $subtract->totalContributionScore;
        $newStats->lastMatchContributionScore = $this->lastMatchContributionScore - $subtract->lastMatchContributionScore;
        $newStats->lastMatchRounds = $this->lastMatchRounds - $subtract->lastMatchRounds;
        $newStats->totalKillsHkp2000 = $this->totalKillsHkp2000 - $subtract->totalKillsHkp2000;
        $newStats->totalShotsHkp2000 = $this->totalShotsHkp2000 - $subtract->totalShotsHkp2000;
        $newStats->totalHitsHkp2000 = $this->totalHitsHkp2000 - $subtract->totalHitsHkp2000;
        $newStats->totalHitsP250 = $this->totalHitsP250 - $subtract->totalHitsP250;
        $newStats->totalKillsP250 = $this->totalKillsP250 - $subtract->totalKillsP250;
        $newStats->totalShotsP250 = $this->totalShotsP250 - $subtract->totalShotsP250;
        $newStats->totalKillsSg556 = $this->totalKillsSg556 - $subtract->totalKillsSg556;
        $newStats->totalShotsSg556 = $this->totalShotsSg556 - $subtract->totalShotsSg556;
        $newStats->totalHitsSg556 = $this->totalHitsSg556 - $subtract->totalHitsSg556;
        $newStats->totalHitsScar20 = $this->totalHitsScar20 - $subtract->totalHitsScar20;
        $newStats->totalKillsScar20 = $this->totalKillsScar20 - $subtract->totalKillsScar20;
        $newStats->totalShotsScar20 = $this->totalShotsScar20 - $subtract->totalShotsScar20;
        $newStats->totalShotsSsg08 = $this->totalShotsSsg08 - $subtract->totalShotsSsg08;
        $newStats->totalHitsSsg08 = $this->totalHitsSsg08 - $subtract->totalHitsSsg08;
        $newStats->totalKillsSsg08 = $this->totalKillsSsg08 - $subtract->totalKillsSsg08;
        $newStats->totalShotsMp7 = $this->totalShotsMp7 - $subtract->totalShotsMp7;
        $newStats->totalHitsMp7 = $this->totalHitsMp7 - $subtract->totalHitsMp7;
        $newStats->totalKillsMp7 = $this->totalKillsMp7 - $subtract->totalKillsMp7;
        $newStats->totalKillsMp9 = $this->totalKillsMp9 - $subtract->totalKillsMp9;
        $newStats->totalShotsMp9 = $this->totalShotsMp9 - $subtract->totalShotsMp9;
        $newStats->totalHitsMp9 = $this->totalHitsMp9 - $subtract->totalHitsMp9;
        $newStats->totalHitsNova = $this->totalHitsNova - $subtract->totalHitsNova;
        $newStats->totalKillsNova = $this->totalKillsNova - $subtract->totalKillsNova;
        $newStats->totalShotsNova = $this->totalShotsNova - $subtract->totalShotsNova;
        $newStats->totalHitsNegev = $this->totalHitsNegev - $subtract->totalHitsNegev;
        $newStats->totalKillsNegev = $this->totalKillsNegev - $subtract->totalKillsNegev;
        $newStats->totalShotsNegev = $this->totalShotsNegev - $subtract->totalShotsNegev;
        $newStats->totalShotsSawedoff = $this->totalShotsSawedoff - $subtract->totalShotsSawedoff;
        $newStats->totalHitsSawedoff = $this->totalHitsSawedoff - $subtract->totalHitsSawedoff;
        $newStats->totalKillsSawedoff = $this->totalKillsSawedoff - $subtract->totalKillsSawedoff;
        $newStats->totalShotsBizon = $this->totalShotsBizon - $subtract->totalShotsBizon;
        $newStats->totalHitsBizon = $this->totalHitsBizon - $subtract->totalHitsBizon;
        $newStats->totalKillsBizon = $this->totalKillsBizon - $subtract->totalKillsBizon;
        $newStats->totalKillsTec9 = $this->totalKillsTec9 - $subtract->totalKillsTec9;
        $newStats->totalShotsTec9 = $this->totalShotsTec9 - $subtract->totalShotsTec9;
        $newStats->totalHitsTec9 = $this->totalHitsTec9 - $subtract->totalHitsTec9;
        $newStats->totalShotsMag7 = $this->totalShotsMag7 - $subtract->totalShotsMag7;
        $newStats->totalHitsMag7 = $this->totalHitsMag7 - $subtract->totalHitsMag7;
        $newStats->totalKillsMag7 = $this->totalKillsMag7 - $subtract->totalKillsMag7;
        $newStats->totalGunGameContributionScore = $this->totalGunGameContributionScore - $subtract->totalGunGameContributionScore;
        $newStats->lastMatchGgContributionScore = $this->lastMatchGgContributionScore - $subtract->lastMatchGgContributionScore;
        $newStats->totalKillsM4a1 = $this->totalKillsM4a1 - $subtract->totalKillsM4a1;
        $newStats->totalKillsGalilar = $this->totalKillsGalilar - $subtract->totalKillsGalilar;
        $newStats->totalKillsMolotov = $this->totalKillsMolotov - $subtract->totalKillsMolotov;
        $newStats->totalShotsM4a1 = $this->totalShotsM4a1 - $subtract->totalShotsM4a1;
        $newStats->totalShotsGalilar = $this->totalShotsGalilar - $subtract->totalShotsGalilar;
        $newStats->totalShotsTaser = $this->totalShotsTaser - $subtract->totalShotsTaser;
        $newStats->totalHitsM4a1 = $this->totalHitsM4a1 - $subtract->totalHitsM4a1;
        $newStats->totalHitsGalilar = $this->totalHitsGalilar - $subtract->totalHitsGalilar;
        $newStats->totalMatchesWonSafehouse = $this->totalMatchesWonSafehouse - $subtract->totalMatchesWonSafehouse;

        return $newStats;
    }
}
