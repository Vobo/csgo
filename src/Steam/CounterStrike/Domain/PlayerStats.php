<?php

namespace Steam\CounterStrike\Domain;

use DateTime;

class PlayerStats
{
    private $id;
    private $playerId;
    private $fetchedAt;

    /** @var SteamStats */
    private $steamStats;

    public function __construct(PlayerId $playerId, SteamStats $steamStats)
    {
        $this->fetchedAt = new DateTime();
        $this->playerId = $playerId->getId();
        $this->steamStats = $steamStats;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPlayerId()
    {
        return $this->playerId;
    }

    public function getFetchedAt()
    {
        return $this->fetchedAt;
    }

    public function getSteamStats()
    {
        return $this->steamStats;
    }

    public function equals(PlayerStats $playerStats = null)
    {
        if (is_null($playerStats)) {
            return false;
        }

        return $this->steamStats->equals($playerStats->steamStats);
    }
}
