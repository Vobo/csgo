<?php

namespace CommonStatsBundle\Domain;

class KDRatio
{
    private $kills;
    private $deaths;

    public function __construct($kills, $deaths)
    {
        $this->kills = $kills;
        $this->deaths = $deaths;
    }

    public function ratio()
    {
        if (0 === $this->deaths) {
            if (0 === $this->kills ) {
                return 0;
            }
            return 'Infinity';
        }

        return round($this->kills / $this->deaths, 2);
    }
}
