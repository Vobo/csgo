<?php

namespace PlayerStatsBundle\Domain;

use CommonStatsBundle\Domain\KDRatio;

class PlayerStats
{
    private $kills;
    private $deaths;
    private $shotsFired;
    private $hitsMade;

    public function __construct($kills, $deaths, $shots, $hits)
    {
        $this->kills = $kills;
        $this->deaths = $deaths;
        $this->shotsFired = $shots;
        $this->hitsMade = $hits;
    }

    public function toArray()
    {
        return [
            'kills' => $this->kills,
            'deaths' => $this->deaths,
            'shotsFired' => $this->shotsFired,
            'hitsMade' => $this->hitsMade,
            'killDeathRatio' => (new KDRatio($this->kills, $this->deaths))->ratio(),
        ];
    }
}