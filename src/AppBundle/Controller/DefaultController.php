<?php

namespace AppBundle\Controller;

use PlayerStatsBundle\Domain\PlayerName;
use PlayerStatsBundle\Domain\PlayerStats as PlayerStatsPlayerDomain;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Steam\CounterStrike\Application\FetchPlayerDataCommand;
use Steam\CounterStrike\Domain\PlayerStats;
use Steam\CounterStrike\Domain\PlayerStatsContainer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


class DefaultController extends Controller
{
    /**
     * @Route("/app/last-match/{playerId}", name="last_match")
     */
    public function indexAction($playerId)
    {
        $ok = json_decode(file_get_contents('http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=730&key=1DC374539866B31C03E0F618A6C61022&steamid=76561197962348102'));
        $stats = $ok->playerstats->stats;

        $lastMatch = array_filter($stats, function ($element) {

            return preg_match('/^last*/', $element->name);
        });

        $names = [];

        foreach ($lastMatch as $lastMatchElement) {
            $name = str_replace('last_match_', '', $lastMatchElement->name);
            $names[$name] = $lastMatchElement->value;
        }

        return new Response(print_r($names, 1));
    }

    /**
     * @Route("/app/fetch/{playerId}", name="fetch")
     *
     * @param string $playerId
     *
     * @return Response
     */
    public function fetchAction($playerId)
    {
        $this->get('steam.counter_strike.stats_service')->fetchNewPlayerData(new FetchPlayerDataCommand($playerId));

        return new Response(null, 204);
    }

    /**
     * @Route("/app/list", name="list")
     *
     * @return Response
     */
    public function listAction()
    {
        $playersStats = $this->get('steam.counter_strike.player_stats.orm_repository')->findAll();

        $playerStatsContainer = new PlayerStatsContainer();

        return $this->render(':stats:player.html.twig', ['players' => array_map(function (PlayerStats $stat) use ($playerStatsContainer) {
            $steamStats = $playerStatsContainer->getDiff($stat);

            $playerId = $stat->getPlayerId();

            $name = (new PlayerName($playerId))->name();

            return [
                'id' => $stat->getId(),
                'fetchedAt' => $stat->getFetchedAt(),
                'player' => [
                    'steamId' => $playerId,
                    'name' => $name,
                ],
                'stats' => (new PlayerStatsPlayerDomain(
                    $steamStats->totalKills,
                    $steamStats->totalDeaths,
                    $steamStats->totalShotsFired,
                    $steamStats->totalShotsHit
                ))->toArray()
            ];
        }, $playersStats)]);

    }
}
