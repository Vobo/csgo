#!/bin/bash

echo "+ remove cache and logs directories"
sudo rm -rf ./app/cache ./app/logs

echo "+ create cache and logs directories"
mkdir ./app/cache
mkdir ./app/logs

echo "+ apply filesystem ACL on cache and logs directories"
sudo setfacl -R -m u:www-data:rwX -m u:`whoami`:rwX ./app/cache ./app/logs
sudo setfacl -dR -m u:www-data:rwX -m u:`whoami`:rwX ./app/cache ./app/logs
